public class DccRootResource {
        private String resourceType;
        private DccDataResource data;
        public List<Object> errors;

        public String getResourceType() {
                return resourceType;
        }

        public void setResourceType(String resourceType) {
                this.resourceType = resourceType;
        }

        public DccDataResource getData() {
                return data;
        }

        public void setData(DccDataResource data) {
                this.data = data;
        }

        public List<Object> getErrors() {
                return errors;
        }

        public void setErrors(List<Object> errors) {
                this.errors = errors;
        }
}
