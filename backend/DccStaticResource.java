public class DccStaticResource {
    private DccHeaderResource header;
    private DccMessageResource message;
    private Object annexe;
    private DccSignatureResource signature;

    public DccHeaderResource getHeader() {
        return header;
    }

    public void setHeader(DccHeaderResource header) {
        this.header = header;
    }

    public DccMessageResource getMessage() {
        return message;
    }

    public void setMessage(DccMessageResource message) {
        this.message = message;
    }

    public Object getAnnexe() {
        return annexe;
    }

    public void setAnnexe(Object annexe) {
        this.annexe = annexe;
    }

    public DccSignatureResource getSignature() {
        return signature;
    }

    public void setSignature(DccSignatureResource signature) {
        this.signature = signature;
    }
}
