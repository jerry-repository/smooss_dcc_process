public enum LogicalDocumentType {
    // DCC Content (from import or qr code scan) validated using IN API
    DCC_CONTENT,
    // Uploaded EU health pass for back-office check if name returned by IN API does not match booking name
    EU_HEALTH_PASS,
    // Uploaded foreign health pass for back-office check
    FOREIGN_HEALTH_PASS,
    // Proof of identity due to an invalid name error for back-office check
    PROOF,
    // Additional document required on some destinations for back-office check
    ADDITIONAL_DOCUMENT
}