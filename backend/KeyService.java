public class KeyService {

    private static final Log LOG = LogFactory.getLog(KeyService.class);
    private PrivateKey rsaPrivateKey;
    private PublicKey rsaPublicKey;

    @PostConstruct
    public void generateKey() {
        KeyPair keys = RSAUtils.generateKey();
        if (keys != null) {
            rsaPublicKey = keys.getPublic();
            rsaPrivateKey = keys.getPrivate();
        }
    }

    public String encodeRSAString(String stringToEncode, Key specificPublicKey) {
        return RSAUtils.encodeString(stringToEncode, specificPublicKey);
    }

    public String decodeRSAString(String stringToDecode) {
        return RSAUtils.decodeString(stringToDecode, rsaPrivateKey);
    }

    public String encodeAESString(String stringToEncode, String aesKey) {

        SecretKeySpec keySpec = new SecretKeySpec(aesKey.substring(0, 16).getBytes(), "AES");
        IvParameterSpec ivSpec = new IvParameterSpec(aesKey.substring(16).getBytes());
        return AESUtils.encodeString(stringToEncode, keySpec, ivSpec);
    }

    public String decodeAESString(String stringToDecode, String aesKey) {
        SecretKeySpec keySpec = new SecretKeySpec(aesKey.substring(0, 16).getBytes(), "AES");
        IvParameterSpec ivSpec = new IvParameterSpec(aesKey.substring(16).getBytes());
        return AESUtils.decodeString(stringToDecode, keySpec, ivSpec);
    }

    public PublicKey getRSAPublicKey() {
        return rsaPublicKey;
    }

    public void setRSAPublicKey(PublicKey publicKey) {
        this.rsaPublicKey = publicKey;
    }

}
