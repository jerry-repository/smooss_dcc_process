export interface DccAnswerResource {
  dccValue: string;
  bookingId: string;
  paxReference: string;
  encodedKey: string;
}