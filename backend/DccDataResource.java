public class DccDataResource {
    private List<DccDynamicResource> dynamic;
    @JsonProperty(value = "static")
    private DccStaticResource dccStatic;
    private List<Object> external;

    public List<DccDynamicResource> getDynamic() {
        return dynamic;
    }

    public void setDynamic(List<DccDynamicResource> dynamic) {
        this.dynamic = dynamic;
    }

    public DccStaticResource getDccStatic() {
        return dccStatic;
    }

    public void setDccStatic(DccStaticResource dccStatic) {
        this.dccStatic = dccStatic;
    }

    public List<Object> getExternal() {
        return external;
    }

    public void setExternal(List<Object> external) {
        this.external = external;
    }
}