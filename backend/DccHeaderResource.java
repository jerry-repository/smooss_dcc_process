public class DccHeaderResource {
    private List<DccFieldResource> fields;

    public List<DccFieldResource> getFields() {
        return fields;
    }

    public void setFields(List<DccFieldResource> fields) {
        this.fields = fields;
    }
}
