    @Operation(summary = "retrieve DCC information")
    @PutMapping(value = "/dcc")
    @ResponseBody
    public DccResponse retrieveDccInformation(@RequestBody DccAnswerResource answer) {
        if (keyService == null) {
            return null;
        }
        DccResponse dccResponse = new DccResponse();
        String dcc = keyService.decodeAESString(answer.getDccValue(), keyService.decodeRSAString(answer.getEncodedKey()));
        // retrieve DB information
        Booking booking = bookingService.findByIdWithChildEntities(answer.getBookingId(), true);
        Campaign campaign = campaignService.getCampaign(booking.getCampaignId());
        Target target = targetService.getOne(booking.getTargetId());
        try (JsonThreadNaming oThread = new JsonThreadNaming(CUSTOMER, campaign, target, booking)) {
            try {
                Passenger currentPax = booking.getBookingSnapshotsList().get(0).getPassengersList().stream().filter(passenger -> passenger.getReference().equals(answer.getPaxReference())).findFirst().orElse(null);
                if (currentPax == null) {
                    LOG.error("Unable to find pax");
                    dccResponse.setResponse(keyService.encodeAESString(DccValidationStatus.UNEXPECTED_ERROR.toString(), keyService.decodeRSAString(answer.getEncodedKey())));
                    return dccResponse;
                }
                // In case the DCC is not already validate for the current pax
                if (!bookingService.alreadyContainsDccValidation(currentPax, booking.getId())) {
                    LOG.info("DCC upload : " + booking.getId() + ", paxRef : " + currentPax.getReference() + ", bookingStatus : " + booking.getStatus());
                    DccValidationStatus dccValid = dccService.isValidDcc(dcc, currentPax, target.getSegments().get(0));
                    if (DccValidationStatus.TRIP_VALID.equals(dccValid) || DccValidationStatus.INVALID_NAME.equals(dccValid)) {
                        booking.setSelectedType(ChoiceType.QR_CODE);
                        booking.setSelectedSubType(ChoiceSubType.HEALTH_PASS);

                        PassengerDocument passengerDocument = new PassengerDocument();
                        passengerDocument.setPassengerReference(currentPax.getReference());
                        passengerDocument.setLogicalDocumentType(LogicalDocumentType.DCC_CONTENT);
                        passengerDocument.setDocumentName("DCC");
                        passengerDocument.setStatus(dccValid);

                        manageBookingStatusForDcc(booking, campaign, target, dccValid, passengerDocument);
                        bookingService.saveBooking(booking);
                    }
                    LOG.info("Dcc validity : " + dccValid.name() + ", bookingId " + booking.getId() + ", paxRef : " + currentPax.getReference() + ", bookingStatus : " + booking.getStatus());
                    dccResponse.setResponse(keyService.encodeAESString(dccValid.toString(), keyService.decodeRSAString(answer.getEncodedKey())));
                } else {
                    LOG.warn("Dcc already valid, bookingId " + booking.getId() + ", paxRef : " + currentPax.getReference());
                    dccResponse.setResponse(keyService.encodeAESString(DccValidationStatus.TRIP_VALID.toString(), keyService.decodeRSAString(answer.getEncodedKey())));
                }
                return dccResponse;
            } catch (Exception e) {
                LOG.error("Error while checking DCC, bookingId : " + answer.getBookingId() + ", paxRef : " + answer.getPaxReference(), e);
                dccResponse.setResponse(keyService.encodeAESString(DccValidationStatus.UNEXPECTED_ERROR.toString(), keyService.decodeRSAString(answer.getEncodedKey())));
                return dccResponse;
            }
        }
    }

    private void manageBookingStatusForDcc(Booking booking, Campaign campaign, Target target, DccValidationStatus dccValid, PassengerDocument passengerDocument) {
        if (DccValidationStatus.TRIP_VALID.equals(dccValid)) {
            bookingService.createPassengerDocument(booking, passengerDocument);
            bookingService.changeBookingStatusToProcessed(booking);
            // Change the booking status to IN_PROGRESS, if this is the first pax validation
            if (BookingStatus.OPENED.equals(booking.getStatus())) {
                BookingUtils.changeBookingStatus(booking, BookingStatus.IN_PROGRESS);
            }
            // If the booking status is PROCESSED we automatically accept the document and update counter
            else if (BookingStatus.PROCESSED.equals(booking.getStatus())) {
                bookingService.acceptPassengerDocumentsInBooking(campaign, target, booking, null);
                booking.setRemark(null);
                if (BookingStatus.ERROR.equals(booking.getStatus())) {
                    booking.removePassengerDocument(passengerDocument);
                    BookingUtils.changeBookingStatus(booking, BookingStatus.IN_PROGRESS);
                } else {
                    targetService.updateTargetMetrics(booking.getTargetId(), booking.getProduct());
                    campaignService.askToUpdateCampaignMetrics(booking.getCampaignId());
                    campaignService.updateAlternativeMetrics(booking.getTargetId());
                }
                bookingService.handleBookingEvent(campaign, target, booking);
            }
        } else if (DccValidationStatus.INVALID_NAME.equals(dccValid)) {
            bookingService.createPassengerDocument(booking, passengerDocument);
        }
    }

