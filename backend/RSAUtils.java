public class RSAUtils {

    private static final Log LOG = LogFactory.getLog(RSAUtils.class);

    private RSAUtils() {
        throw new IllegalStateException("Utility class");
    }

    public static KeyPair generateKey() {
        try {
            KeyPairGenerator generator = KeyPairGenerator.getInstance("RSA");
            generator.initialize(3072);
            return generator.generateKeyPair();
        } catch (NoSuchAlgorithmException e) {
            LOG.error("Unable to create RSA keys", e);
        }
        return null;
    }

    public static String encodeString(String stringToEncode, Key publicKey) {
        try {
            Cipher encryptCipher = Cipher.getInstance("RSA");
            encryptCipher.init(Cipher.ENCRYPT_MODE, publicKey);
            byte[] secretMessageBytes = stringToEncode.getBytes(StandardCharsets.UTF_8);
            byte[] encryptedMessageBytes = encryptCipher.doFinal(secretMessageBytes);
            return Base64.getEncoder().encodeToString(encryptedMessageBytes);
        } catch (NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException | IllegalBlockSizeException | BadPaddingException e) {
            LOG.error("Unable to Encode string", e);
        }
        return null;
    }

    public static String decodeString(String stringToDecode, Key privateKey) {
        try {
            Cipher decryptCipher = Cipher.getInstance("RSA");
            decryptCipher.init(Cipher.DECRYPT_MODE, privateKey);
            byte[] decryptedMessageBytes = decryptCipher.doFinal(Base64.getDecoder().decode(stringToDecode));
            return new String(decryptedMessageBytes, StandardCharsets.UTF_8);
        } catch (NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException | IllegalBlockSizeException | BadPaddingException e) {
            LOG.error("Unable to Encode string", e);
        }
        return null;
    }
}
