@Component
public class DeleteDocumentScheduled {
    @Autowired
    private CampaignService campaignService;

    @Autowired
    private TargetService targetService;

    @Autowired
    private BookingService bookingService;

    @Autowired
    private GeographicalDataService geographicalDataService;

    @Autowired
    private JobExecutionService jobExecutionService;

    @Autowired
    private PssServiceLoader pssService;

    private static final Log LOG = LogFactory.getLog(DeleteDocumentScheduled.class);

    @Scheduled(cron = "0 8/10 * * * *")
    public void deleteDocument() {

        ScheduleJobExecution scheduleJobExecution = new ScheduleJobExecution();
        scheduleJobExecution.setStatus(JobStatus.STARTED);
        scheduleJobExecution.setStartTime(LocalDateTime.now());
        scheduleJobExecution.setScheduleName(this.getClass().getSimpleName());
        scheduleJobExecution = jobExecutionService.save(scheduleJobExecution);


        LOG.info("Deleting documents - Looking for bookings with target that a already passed");
        try {
            List<Booking> bookingsWithDocuments = bookingService.findBookingsWithSliceNearlyDepartedAndWithPassengerDocument();

            if (CollectionUtils.isEmpty(bookingsWithDocuments)) {
                LOG.info("Deleting documents - No booking found");
                return;
            }

            Map<Long, List<Booking>> bookingsByTargetId = bookingsWithDocuments.stream().collect(Collectors.groupingBy(Booking::getTargetId, Collectors.mapping(booking -> booking, Collectors.toList())));
            bookingsByTargetId.forEach((targetId, bookings) -> {
                LOG.info("Deleting documents - Looking targetId : " + targetId);
                // retrieve target
                Target target = targetService.findById(targetId);
                for (Booking booking : bookings) {
                    LOG.info("Deleting documents - Looking Booking : " + booking.getId());
                    bookingService.deletePassengerDocumentsByBooking(booking);
                    LOG.info("Deleting documents - for Booking : " + booking.getId() + ", is completed");
                    booking = bookingService.findById(booking.getId());
                    if (BookingStatus.ACCEPTED.equals(booking.getStatus()) || BookingStatus.ADDRESSED.equals(booking.getStatus()) || BookingStatus.IN_PROGRESS.equals(booking.getStatus())) {
                        booking = bookingService.findByIdWithChildEntities(booking.getId(), false);
                        BookingUtils.changeBookingStatus(booking, BookingStatus.DENIED);
                        bookingService.saveBooking(booking);
                        targetService.updateTargetMetrics(targetId, booking.getProduct());
                        campaignService.askToUpdateCampaignMetrics(booking.getCampaignId());
                        pssService.setRemarksOnTargetSlice(booking.getPnr(), target, booking, RemarkContext.OFFER_DENIED);
                    }
                }
            });

        } catch (Exception e) {
            scheduleJobExecution.setStatus(JobStatus.FAILED);
            scheduleJobExecution.setExitMessage((scheduleJobExecution.getExitMessage() == null ? "" : scheduleJobExecution.getExitMessage()) + "Error occurred while deleting document : \n " + e.getMessage() + "\n");
            LOG.error("Error occurred while deleting document", e);
        }
        scheduleJobExecution.setEndTime(LocalDateTime.now());
        if (!JobStatus.FAILED.equals(scheduleJobExecution.getStatus())) {
            scheduleJobExecution.setStatus(JobStatus.COMPLETED);
        }
        scheduleJobExecution.setExitCode(scheduleJobExecution.getStatus().name());
        jobExecutionService.save(scheduleJobExecution);
    }
}
