export enum LogicalDocumentType {
  // Standard DCC (from import or qr code scan)
  DCC_TYPE_1 = "DCC_TYPE_1",
  // Uploaded DCC after an invalid name error
  DCC_TYPE_2 = "DCC_TYPE_2",
  // Foreign DCC
  EQUIVALENT_DCC = "EQUIVALENT_DCC",
  // Required proof document due to an invalid name error
  PROOF = "PROOF",
  // Additional document required by QR_CODE_UPLOAD choiceType
  ADDITIONAL_DOCUMENT = "ADDITIONAL_DOCUMENT"
}