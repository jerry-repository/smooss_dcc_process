public class DccDynamicResource {
        private String liteVaccinePassValidityStatus;
        private String liteHealthPassValidityStatus;
        private String liteFirstName;
        private String liteLastName;
        private String liteDateOfBirth;
        private String otOutValidityStatus;
        private String otInGreenValidityStatus;
        private String otInOrangeValidityStatus;
        private String otInRedValidityStatus;
        private String otInValidityStatus;
        private String otInGreenTestValidity;
        private String otInOrangeTestValidity;
        private String otInRedTestValidity;
        private String otTestDuration;
        private String otTestResultLabel;
        private String otTestTypeLabel;
        private String otTestManufacturerLabel;
        private String otVaccinationDuration;
        private String otVaccinationMedicalProductLabel;
        private String otVaccinationManufacturerLabel;
        private String otVaccinationProphylaxisLabel;

        public String getLiteVaccinePassValidityStatus() {
                return liteVaccinePassValidityStatus;
        }

        public void setLiteVaccinePassValidityStatus(String liteVaccinePassValidityStatus) {
                this.liteVaccinePassValidityStatus = liteVaccinePassValidityStatus;
        }

        public String getLiteHealthPassValidityStatus() {
                return liteHealthPassValidityStatus;
        }

        public void setLiteHealthPassValidityStatus(String liteHealthPassValidityStatus) {
                this.liteHealthPassValidityStatus = liteHealthPassValidityStatus;
        }

        public String getLiteFirstName() {
                return liteFirstName;
        }

        public void setLiteFirstName(String liteFirstName) {
                this.liteFirstName = liteFirstName;
        }

        public String getLiteLastName() {
                return liteLastName;
        }

        public void setLiteLastName(String liteLastName) {
                this.liteLastName = liteLastName;
        }

        public String getLiteDateOfBirth() {
                return liteDateOfBirth;
        }

        public void setLiteDateOfBirth(String liteDateOfBirth) {
                this.liteDateOfBirth = liteDateOfBirth;
        }

        public String getOtOutValidityStatus() {
                return otOutValidityStatus;
        }

        public void setOtOutValidityStatus(String otOutValidityStatus) {
                this.otOutValidityStatus = otOutValidityStatus;
        }

        public String getOtInGreenValidityStatus() {
                return otInGreenValidityStatus;
        }

        public void setOtInGreenValidityStatus(String otInGreenValidityStatus) {
                this.otInGreenValidityStatus = otInGreenValidityStatus;
        }

        public String getOtInOrangeValidityStatus() {
                return otInOrangeValidityStatus;
        }

        public void setOtInOrangeValidityStatus(String otInOrangeValidityStatus) {
                this.otInOrangeValidityStatus = otInOrangeValidityStatus;
        }

        public String getOtInRedValidityStatus() {
                return otInRedValidityStatus;
        }

        public void setOtInRedValidityStatus(String otInRedValidityStatus) {
                this.otInRedValidityStatus = otInRedValidityStatus;
        }

        public String getOtInGreenTestValidity() {
                return otInGreenTestValidity;
        }

        public void setOtInGreenTestValidity(String otInGreenTestValidity) {
                this.otInGreenTestValidity = otInGreenTestValidity;
        }

        public String getOtInOrangeTestValidity() {
                return otInOrangeTestValidity;
        }

        public void setOtInOrangeTestValidity(String otInOrangeTestValidity) {
                this.otInOrangeTestValidity = otInOrangeTestValidity;
        }

        public String getOtInRedTestValidity() {
                return otInRedTestValidity;
        }

        public void setOtInRedTestValidity(String otInRedTestValidity) {
                this.otInRedTestValidity = otInRedTestValidity;
        }

        public String getOtTestDuration() {
                return otTestDuration;
        }

        public void setOtTestDuration(String otTestDuration) {
                this.otTestDuration = otTestDuration;
        }

        public String getOtTestResultLabel() {
                return otTestResultLabel;
        }

        public void setOtTestResultLabel(String otTestResultLabel) {
                this.otTestResultLabel = otTestResultLabel;
        }

        public String getOtTestTypeLabel() {
                return otTestTypeLabel;
        }

        public void setOtTestTypeLabel(String otTestTypeLabel) {
                this.otTestTypeLabel = otTestTypeLabel;
        }

        public String getOtTestManufacturerLabel() {
                return otTestManufacturerLabel;
        }

        public void setOtTestManufacturerLabel(String otTestManufacturerLabel) {
                this.otTestManufacturerLabel = otTestManufacturerLabel;
        }

        public String getOtInValidityStatus() {
                return otInValidityStatus;
        }

        public void setOtInValidityStatus(String otInValidityStatus) {
                this.otInValidityStatus = otInValidityStatus;
        }

        public String getOtVaccinationDuration() {
                return otVaccinationDuration;
        }

        public void setOtVaccinationDuration(String otVaccinationDuration) {
                this.otVaccinationDuration = otVaccinationDuration;
        }

        public String getOtVaccinationMedicalProductLabel() {
                return otVaccinationMedicalProductLabel;
        }

        public void setOtVaccinationMedicalProductLabel(String otVaccinationMedicalProductLabel) {
                this.otVaccinationMedicalProductLabel = otVaccinationMedicalProductLabel;
        }

        public String getOtVaccinationManufacturerLabel() {
                return otVaccinationManufacturerLabel;
        }

        public void setOtVaccinationManufacturerLabel(String otVaccinationManufacturerLabel) {
                this.otVaccinationManufacturerLabel = otVaccinationManufacturerLabel;
        }

        public String getOtVaccinationProphylaxisLabel() {
                return otVaccinationProphylaxisLabel;
        }

        public void setOtVaccinationProphylaxisLabel(String otVaccinationProphylaxisLabel) {
                this.otVaccinationProphylaxisLabel = otVaccinationProphylaxisLabel;
        }
}
