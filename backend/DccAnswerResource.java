public class DccAnswerResource {
    private String dccValue;
    private String bookingId;
    private String paxReference;
    private String encodedKey;

    public String getDccValue() {
        return dccValue;
    }

    public void setDccValue(String dccValue) {
        this.dccValue = dccValue;
    }

    public String getBookingId() {
        return bookingId;
    }

    public void setBookingId(String bookingId) {
        this.bookingId = bookingId;
    }

    public String getPaxReference() {
        return paxReference;
    }

    public void setPaxReference(String paxReference) {
        this.paxReference = paxReference;
    }

    public String getEncodedKey() {
        return encodedKey;
    }

    public void setEncodedKey(String encodedKey) {
        this.encodedKey = encodedKey;
    }
}
