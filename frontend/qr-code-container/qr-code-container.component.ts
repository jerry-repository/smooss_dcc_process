import { STEPPER_GLOBAL_OPTIONS } from "@angular/cdk/stepper";
import { Component, EventEmitter, Input, OnChanges, OnDestroy, OnInit, Output, SimpleChanges, ViewChild } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { MatStepper } from "@angular/material/stepper";
import { Booking, BookingUtils, BookingPassengerChoice, BookingStatus, CompanyInfo, CustomerChoice, CustomerTextContent, DccAnswerResource, DccValidationStatus, Language, LogicalDocumentType, OfferChoiceDetail, ChoiceType, CustomerBookingPassengerDocument, DccResponse } from "@jerry/model";
import { TranslateService } from "@ngx-translate/core";
import { ZXingScannerComponent } from "@zxing/ngx-scanner";
import { Subject } from "rxjs";
import { take, takeUntil } from "rxjs/operators";
import { CustomerComponentDirective } from "../shared/directive/customer.component";
import { DccService } from "../services/dcc.service";
import { getContextualLanguage } from "../utils/utils";
import { BarcodeFormat } from '@zxing/library';
import { BrowserQRCodeReader } from "@zxing/browser";
import { CustomerBookingService } from "../services/customer.booking.service";
import { KeyService } from './../services/key.service';
import * as CryptoJS from 'crypto-js';

@Component({
  selector: 'jerry-qr-code-container',
  templateUrl: './qr-code-container.component.html',
  styleUrls: ['./qr-code-container.component.scss'],
  providers: [{
    provide: STEPPER_GLOBAL_OPTIONS, useValue: { displayDefaultIndicatorType: false }
  }]
})
export class QrCodeContainerComponent extends CustomerComponentDirective implements OnInit, OnDestroy, OnChanges {

  @Input() currentLang: string;
  @Input() companyInfo: CompanyInfo;
  @Input() booking: Booking;
  @Input() product: string;

  @Output() emitReloadBooking = new EventEmitter();

  private _unsubscribeAll: Subject<any>;
  private loadedLanguages: string[];

  // Scanner
  isEnable: boolean;
  autostart: boolean;
  isLinear: boolean;
  allowedFormats: BarcodeFormat[];
  availableDevices!: MediaDeviceInfo[];
  selectedDevice: MediaDeviceInfo | undefined;

  // General
  isLoading: boolean;
  isValidStep2: boolean;
  consentForm!: FormGroup;
  globalErrorMessage: string;
  passengers: BookingPassengerChoice[];
  currentPassenger: BookingPassengerChoice;
  currentOffer: OfferChoiceDetail;

  // Standard use case
  dccByPassenger: Map<number, string>;

  // Error use case
  errorByPassenger: Map<number, DccValidationStatus>;
  uploadedDccByPassenger: Map<number, { content: string, name: string, type: string }>;
  uploadedProofByPassenger: Map<number, { content: string, name: string, type: string }>;

  // QR_CODE_UPLOAD use case
  uploadedAdditionalDocumentsByPassenger: Map<number, { content: string, name: string, type: string }[]>;

  secretKey: String;
  iv: String;
  secretBase: String;
  ivBase : String;
  // More information at https://github.com/zxing-js/ngx-scanner
  @ViewChild('scanner', { static: true }) scanner!: ZXingScannerComponent;
  @ViewChild('stepper') stepper: MatStepper;

  constructor(
    private fb: FormBuilder,
    private dccService: DccService,
    private customerBookingService: CustomerBookingService,
    private translateService: TranslateService,
    protected keyService: KeyService

  ) {
    super(translateService);

    this.isLinear = true;
    this.autostart = false;
    this.isEnable = false;
    this.isLoading = false;
    this.isValidStep2 = false;

    // More information at https://github.com/zxing-js/ngx-scanner/wiki/Getting-Started
    this.allowedFormats = [
      BarcodeFormat.AZTEC,
      BarcodeFormat.QR_CODE
    ];

    this._unsubscribeAll = new Subject<any>();
    this.loadedLanguages = [];

    this.dccByPassenger = new Map<number, string>();
    this.errorByPassenger = new Map<number, DccValidationStatus>();
    this.uploadedDccByPassenger = new Map<number, { content: string, name: string, type: string }>();
    this.uploadedProofByPassenger = new Map<number, { content: string, name: string, type: string }>();
    this.uploadedAdditionalDocumentsByPassenger = new Map<number, { content: string, name: string, type: string }[]>();
  }

  // --------------------------------------------------
  // Lifecycle Hooks
  // --------------------------------------------------

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.companyInfo && !this.translateService.currentLang) {
      // this language will be used as a fallback when a translation isn't found in the current language
      this.translateService.setDefaultLang(getContextualLanguage(this.companyInfo, 'en'));
    }

    if (changes.currentLang) {
      const currentLang = changes.currentLang.currentValue;
      if (currentLang && !this.loadedLanguages.includes(currentLang)) {
        this.loadedLanguages = [...this.loadedLanguages, currentLang];
      }
    }

    if (changes.booking) {
      // Filter on passengers (without INF and CHD)
      this.passengers = this.booking.passengers.filter(p => !['CHD', 'INF'].includes(p.passengerType));

      // FIXME : define how to choose the offer
      this.currentOffer = this.booking.offers[0];

      // Pre-check all passengers
      this.passengers.forEach(passenger => this._checkPassengerDocuments(passenger));

      this.isLoading = false;
    }
  }

  ngOnInit(): void {
    this.secretBase = CryptoJS.lib.WordArray.random(8).toString();
    this.ivBase = CryptoJS.lib.WordArray.random(8).toString();
    this.secretKey = CryptoJS.enc.Latin1.parse(this.secretBase);
    this.iv = CryptoJS.enc.Latin1.parse(this.ivBase);
    this.consentForm = this.fb.group({
      'consent': [false, Validators.requiredTrue]
    });

    // Start scanner when a device has been selected
    this.scanner.deviceChange
      .pipe(takeUntil(this._unsubscribeAll))
      .subscribe(() => {
        if (this.isEnable) {
          this.scanner.scanStart();
        }
      });
  }

  ngOnDestroy(): void {
    this._unsubscribeAll.next();
    this._unsubscribeAll.complete();
  }

  // --------------------------------------------------
  // Publics Methods
  // --------------------------------------------------

  /**
   * @description scan availables devices to scan qrCodes 
   * @param devices Availables devices
   */
  onCamerasFound(devices: MediaDeviceInfo[]): void {
    this.availableDevices = devices;
    this.selectedDevice = devices[0];
  }

  /**
   * @description Callback after a qr Code scan succeed. Check the DCC
   * @param scanResult The data of the qr Code
   */
  onScanSuccess(scanResult: string): void {
    this.isEnable = false;
    this.scanner.scanStop();
    this.selectedDevice = undefined;

    this._checkDcc(scanResult);
  }

  /**
   * @description Triggered when clicking on 'Scanner'. Launch the qr Code scanner.
   * @param passenger the current passenger
   */
  onScanQrCode(passenger: BookingPassengerChoice): void {
    // Reset error message
    this._resetError(passenger);

    // Can use Camera ? This methods triggers a deviceChange Event (see ngOnInit)
    this.scanner
      .askForPermission()
      .then(hasPermission => {
        if (hasPermission) {
          this.currentPassenger = passenger;
          this.isEnable = true;
        }
      });
  }

  /**
   * @description Triggered when clicking on 'Upload' btn. Get qr Code from upload document and check the DCC.
   * @param passenger the current passenger
   * @param fileList the uploaded dcc
   */
  onUploadQrCode(passenger: BookingPassengerChoice, fileList: FileList): void {
    if (fileList) {
      this.currentPassenger = passenger;

      const file = fileList[0];
      const fileReader: FileReader = new FileReader();

      fileReader.readAsDataURL(file);

      fileReader.onloadend = (event) => {
        const codeReader = new BrowserQRCodeReader();

        codeReader
          .decodeFromImageUrl(fileReader.result.toString())
          .then(result => {
            this._checkDcc(result.getText());
          });
      };
    }
  }

  onCancelScanQrCode(): void {
    this.isEnable = false;
  }

  getFormattedFlightDescription(): string {
    let result = 'N/A';

    if (this.booking) {
      result = this.booking.flight;
      result += ' ' + this.booking.segments[0].originCode + '-' + this.booking.segments[this.booking.segments.length - 1].destinationCode;
      result += ' ' + (this.currentLang.includes('/fr') ? this.booking.departureDateFr : this.booking.departureDateEn);
    }

    return result;
  }

  onUploadProofDocument(passenger: BookingPassengerChoice, fileList: FileList): void {
    if (fileList) {
      this.currentPassenger = passenger;

      const file = fileList[0];
      const fileReader: FileReader = new FileReader();

      fileReader.onloadend = (event) => {
        const document = {
          content: fileReader.result.toString(),
          name: file.name,
          type: file.type
        };

        this.uploadedProofByPassenger.set(passenger.passengerIndex, document);
        this._addPassengerDocument(document, LogicalDocumentType.PROOF);
      };

      fileReader.readAsDataURL(file);
    }
  }

  onUploadDccDocument(passenger: BookingPassengerChoice, fileList: FileList): void {
    if (fileList) {
      this.currentPassenger = passenger;

      const file = fileList[0];
      const fileReader: FileReader = new FileReader();

      fileReader.onloadend = (event) => {
        const document = {
          content: fileReader.result.toString(),
          name: file.name,
          type: file.type
        };

        this.uploadedDccByPassenger.set(passenger.passengerIndex, document);
        this._addPassengerDocument(document, LogicalDocumentType.EU_HEALTH_PASS);
      };

      fileReader.readAsDataURL(file);
    }
  }

  onUploadAdditionalDocument(passenger: BookingPassengerChoice, fileList: FileList): void {
    if (fileList) {
      this.currentPassenger = passenger;

      const file = fileList[0];
      const fileReader: FileReader = new FileReader();

      fileReader.onloadend = (event) => {
        const document = {
          content: fileReader.result.toString(),
          name: file.name,
          type: file.type
        };

        const existingDocuments = this.uploadedAdditionalDocumentsByPassenger.get(passenger.passengerIndex) || [];

        existingDocuments.push(document);

        this.uploadedAdditionalDocumentsByPassenger.set(passenger.passengerIndex, existingDocuments);
        this._addPassengerDocument(document, LogicalDocumentType.ADDITIONAL_DOCUMENT);
      };

      fileReader.readAsDataURL(file);
    }
  }

  onUploadEquivalentQrCode(passenger: BookingPassengerChoice, fileList: FileList): void {
    if (fileList) {
      this.currentPassenger = passenger;

      const file = fileList[0];
      const fileReader: FileReader = new FileReader();

      fileReader.onloadend = (event) => {
        const document = {
          content: fileReader.result.toString(),
          name: file.name,
          type: file.type
        };

        this.dccByPassenger.set(passenger.passengerIndex, 'OK');
        this._addPassengerDocument(document, LogicalDocumentType.FOREIGN_HEALTH_PASS);
      };

      fileReader.readAsDataURL(file);
    }
  }

  onSubmit(isAdditionalStep: boolean): void {
    if (ChoiceType.QR_CODE_UPLOAD === this.currentOffer.choiceType && !isAdditionalStep) {
      this.isValidStep2 = true;
      // Sync event after html rendering (due to isValidStep2)
      setTimeout(() => this.stepper.next(), 0);
    } else {
      this.isLoading = true;

      const answer: CustomerChoice = {
        bookingId: this.booking.id,
        booking: this.booking,
        product: this.product,
        status: BookingStatus.ACCEPTED,
        selectedOffers: [this.booking.offers[0]],
        language: this.currentLang.split('/')[1].toUpperCase(),
        token: this.booking.token
      };

      this.customerBookingService.updateBooking(answer).pipe(take(1)).subscribe(booking => {
        this.emitReloadBooking.emit();
      }, () => {
        this.emitReloadBooking.emit();
      });
    }
  }

  /**
   * @description Get the content of the current offer according to the selected language
   * @returns The content of the current offer
   */
  getTextContent(): CustomerTextContent {
    let textContent: CustomerTextContent;

    if (this.currentOffer) {
      // FIXME : define how to chosse the offer content
      const currentContent = this.currentOffer.offersContents[0];

      if (this.currentLang.endsWith('fr')) {
        textContent = currentContent.textContents.find(tc => tc.language === Language.FR);
      } else if (this.currentLang.endsWith('en')) {
        textContent = currentContent.textContents.find(tc => tc.language === Language.EN);
      }

      textContent = currentContent.textContents.find(tc => tc.language === Language.EN);
    }

    if (textContent) {
      // Check if terms content has links ( [ url || displayed text ] )
      let links = textContent.terms.match(/[\[]+(.*)[\]]+/gm);

      if (links && links.length > 0) {
        links.forEach(l => {
          let url = l.replace('[', '').replace(']', '').split('||')[0];
          let displayedText = l.replace('[', '').replace(']', '').split('||')[1];

          if (url && displayedText) {
            // Create <a> html tag with url and displayed text
            textContent.terms = textContent.terms.replace(l, "<a href='" + url + "' target='_blank'>" + displayedText + "</a>");
          }
        });
      }
    }

    return textContent;
  }

  isValidPassenger(passenger: BookingPassengerChoice, bypassChoiceAdditionalUpload: boolean): boolean {
    let isValid = false;
    const pIndex = passenger.passengerIndex;

    if ((this.dccByPassenger.has(pIndex))
      || (
        this.errorByPassenger.has(pIndex)
        && this.errorByPassenger.get(pIndex) === DccValidationStatus.INVALID_NAME
        && this.uploadedDccByPassenger.has(pIndex)
        && this.uploadedProofByPassenger.has(pIndex)
      )
    ) {
      if (ChoiceType.QR_CODE_UPLOAD === this.currentOffer.choiceType) {
        if (bypassChoiceAdditionalUpload) {
          isValid = true;
        } else {
          if (this.uploadedAdditionalDocumentsByPassenger.has(pIndex) && this.uploadedAdditionalDocumentsByPassenger.get(pIndex).length > 0) {
            isValid = true;
          }
        }
      } else {
        isValid = true;
      }
    }

    return isValid;
  }

  areValidsPassengers(bypassChoiceAdditionalUpload: boolean): boolean {
    const checkedPassengers = this.passengers.filter(p => this.isValidPassenger(p, bypassChoiceAdditionalUpload));

    return checkedPassengers.length === this.passengers.length;
  }

  // --------------------------------------------------
  // Privates Methods
  // --------------------------------------------------
  private _checkDcc(dcc: string): void {
    this.keyService.getPublicKey()
      .pipe(take(1))
      .subscribe(
        key => {
          var encrypt = this.keyService.encryptAES(dcc, this.secretKey, this.iv);
          var keyEncrypted = this.keyService.encryptRSA(this.secretBase.toString() + this.ivBase.toString(), key.publicKey)
          this.isLoading = true;

          const request: DccAnswerResource = {
            bookingId: this.booking.id,
            dccValue: encrypt.toString(),
            paxReference: this.currentPassenger.pssReference,
            encodedKey: keyEncrypted
          };

          this.dccService.checkDcc(request)
            .pipe(take(1))
            .subscribe((dccResponse: DccResponse) => {

              var decodeValue = this.keyService.decrypt(dccResponse.response.toString(), this.secretKey, this.iv).toString(CryptoJS.enc.Utf8)
              var dccValidationStatus = DccValidationStatus[decodeValue.toString()];

              this.globalErrorMessage = this._getErrorLabelFromDccValidationStatus(dccValidationStatus);

              if (!this.globalErrorMessage || this.globalErrorMessage.length === 0) {
                this._resetError(this.currentPassenger);

                this.dccByPassenger.set(this.currentPassenger.passengerIndex, dcc);
              } else {
                this.errorByPassenger.set(this.currentPassenger.passengerIndex, dccValidationStatus);
              }

              this.isLoading = false;
            });
        })
  }

  /**
   * @description Check if passenger has already submitted a document/exemption/DCC
   * if true : set fake data to bypass 'sending informations' step
   * @param passenger the selected passenger
   */
  private _checkPassengerDocuments(passenger: BookingPassengerChoice): void {
    if (this.booking.passengersDocuments) {
      const existingDocuments = this.booking.passengersDocuments.filter(doc => doc.paxReference === passenger.pssReference);

      if (existingDocuments && existingDocuments.length > 0) {
        const dcc = existingDocuments.find(doc => doc.logicalDocumentType === LogicalDocumentType.DCC_CONTENT);
        const equivalentDcc = existingDocuments.find(doc => doc.logicalDocumentType === LogicalDocumentType.FOREIGN_HEALTH_PASS);
        const uploadedDcc = existingDocuments.find(doc => doc.logicalDocumentType === LogicalDocumentType.EU_HEALTH_PASS);
        const uploadedProof = existingDocuments.find(doc => doc.logicalDocumentType === LogicalDocumentType.PROOF);
        const additionalsUploads = existingDocuments.filter(doc => doc.logicalDocumentType === LogicalDocumentType.ADDITIONAL_DOCUMENT);

        if (dcc || equivalentDcc) {
          this.dccByPassenger.set(passenger.passengerIndex, 'DCC OK');
        }

        if ('QR_CODE_UPLOAD' === this.currentOffer.choiceType && additionalsUploads) {
          const documents = [];

          additionalsUploads.forEach(au => documents.push({ content: au.documentContent, name: au.documentName, type: au.documentType }));

          this.uploadedAdditionalDocumentsByPassenger.set(passenger.passengerIndex, documents);
        }

        if (uploadedProof) {
          this.uploadedProofByPassenger.set(passenger.passengerIndex, { content: uploadedProof.documentContent, name: uploadedProof.documentName, type: uploadedProof.documentType });
          this.errorByPassenger.set(passenger.passengerIndex, DccValidationStatus.INVALID_NAME);
        }

        if (uploadedDcc) {
          this.uploadedDccByPassenger.set(passenger.passengerIndex, { content: uploadedDcc.documentContent, name: uploadedDcc.documentName, type: uploadedDcc.documentType });
          this.errorByPassenger.set(passenger.passengerIndex, DccValidationStatus.INVALID_NAME);
        }
      }
    }
  }

  private _addPassengerDocument(document: any, logicalDocumentType: LogicalDocumentType): void {
    this.isLoading = true;

    // Reset global error message
    this.globalErrorMessage = null;

    const existingDocuments = BookingUtils.extractPassengerUploadedDocumentsInBooking(this.booking, this.currentPassenger) || [];

    const customerPassengerDocumentResource: CustomerBookingPassengerDocument = {
      bookingId: this.booking.id,
      sequenceNumber: existingDocuments.length,
      passengerReference: this.currentPassenger.pssReference,
      passengerTypology: this.currentPassenger.typology,
      documentType: document.type,
      documentName: document.name,
      logicalDocumentType: logicalDocumentType
    };

    this.customerBookingService.savePassengerDocumentInBooking(customerPassengerDocumentResource, document.content)
      .pipe(take(1))
      .subscribe(result => {
        this.isLoading = false;
      });
  }

  private _resetError(passenger: BookingPassengerChoice): void {
    // Reset global error message
    this.globalErrorMessage = null;

    // Reset error for the current passenger
    this.errorByPassenger.delete(passenger.passengerIndex);
  }

  private _getErrorLabelFromDccValidationStatus(status: DccValidationStatus): string {
    if (status) {
      switch (status) {
        case DccValidationStatus.INVALID_NAME:
          return 'dcc_error_qr_code_wrong_matching';
        case DccValidationStatus.TRIP_INVALID:
          return 'dcc_error_qr_code_invalid';
        case DccValidationStatus.BLACKLISTED:
          return 'dcc_error_qr_code_blacklisted';
        case DccValidationStatus.UNEXPECTED_ERROR:
          return 'dcc_error_unexpected_error';
        case DccValidationStatus.UNRECOGNIZED_QR:
          return 'dcc_error_qr_code_not_recognized';
      }
    }

    return null;
  }
}
