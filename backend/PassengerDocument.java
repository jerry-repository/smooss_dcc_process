public class PassengerDocument implements Serializable {
    private static final Log LOG = LogFactory.getLog(PassengerDocument.class);

    @Column
    @ApiModelProperty(notes = "The database generated ID")
    private Long id;

    @Column
    @JsonSerialize(converter = LocalDateTimeSerializer.class)
    @JsonDeserialize(converter = LocalDateTimeDeserializer.class)
    @ApiModelProperty(notes = "Last update of the campaign data")
    private LocalDateTime lastUpdate;

    @Column
    @ApiModelProperty(notes = "Sequence number")
    private Integer sequenceNumber;

    @Column
    @ApiModelProperty(notes = "Name of the current document. The document can be transformed into an image")
    private String documentName;

    private String s3DocumentName;

    @Column
    @Convert(converter = MimeTypeConverter.class)
    @Enumerated(EnumType.STRING)
    @ApiModelProperty(notes = "Type of the current document. The document can be transformed into an image")
    private MimeType documentType;

    @Column
    @ApiModelProperty(notes = "Identifier of the original document, if the document has been converted.")
    private Long originalDocumentId;

    @Column
    @ApiModelProperty(notes = "Passenger reference")
    private String passengerReference;

    @Column
    @Convert(converter = PassengerTypeConverter.class)
    @Enumerated(EnumType.STRING)
    @ApiModelProperty(notes = "Passenger typology")
    private PassengerType passengerTypology;

    @Column
    @ApiModelProperty(notes = "Page index")
    private Integer pageIndex;

    @ApiModelProperty(notes = "Indicates if the document is a native one, not concerned by a conversion")
    private Boolean original;

    @ApiModelProperty(notes = "The document is archived/obsolete")
    @Column
    private Boolean archived;

    @ApiModelProperty(notes = "booking")
    private Booking booking;

    @Column
    @ApiModelProperty(notes = "The logical representation")
    private LogicalDocumentType logicalDocumentType;

    @Column
    private DccValidationStatus status;

    public PassengerDocument() {
    }

    @Id
    @Column(name = "PASSENGER_DOCUMENT_ID")
    @SequenceGenerator(name = "PASSENGER_DOCUMENT_GEN", sequenceName = "SEQ_PASSENGER_DOCUMENT_GEN", allocationSize = 1)
    @GeneratedValue(generator = "PASSENGER_DOCUMENT_GEN", strategy = GenerationType.SEQUENCE)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Convert(converter = DatabaseStringEncrypter.class)
    public String getDocumentName() {
        return documentName;
    }

    @Convert(converter = DatabaseStringEncrypter.class)
    public void setDocumentName(String documentName) {
        this.documentName = documentName;
    }

    @Transient
    public String getS3DocumentName() {
        return s3DocumentName;
    }

    public void setS3DocumentName(String s3DocumentName) {
        this.s3DocumentName = s3DocumentName;
    }

    public LocalDateTime getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(LocalDateTime lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    public Integer getSequenceNumber() {
        return sequenceNumber;
    }

    public void setSequenceNumber(Integer sequenceNumber) {
        this.sequenceNumber = sequenceNumber;
    }

    @Enumerated(EnumType.STRING)
    public MimeType getDocumentType() {
        return documentType;
    }

    public void setDocumentType(MimeType documentType) {
        this.documentType = documentType;
    }

    public Long getOriginalDocumentId() {
        return originalDocumentId;
    }

    public void setOriginalDocumentId(Long originalDocumentId) {
        this.originalDocumentId = originalDocumentId;
    }

    public String getPassengerReference() {
        return passengerReference;
    }

    public void setPassengerReference(String passengerReference) {
        this.passengerReference = passengerReference;
    }

    @Enumerated(EnumType.STRING)
    public PassengerType getPassengerTypology() {
        return passengerTypology;
    }

    public void setPassengerTypology(PassengerType passengerTypology) {
        this.passengerTypology = passengerTypology;
    }

    public Integer getPageIndex() {
        return pageIndex;
    }

    public void setPageIndex(Integer pageIndex) {
        this.pageIndex = pageIndex;
    }

    @Transient
    public Boolean isOriginal() {
        Boolean result = true;
        if (originalDocumentId != null) {
            result = false;
        }
        return result;
    }

    public void setOriginal(Boolean original) {
        this.original = original;
    }

    public Boolean isArchived() {
        Boolean result = archived;
        if (result == null) {
            result = false;
        }
        return result;
    }

    public void setArchived(Boolean archived) {
        this.archived = archived;
    }

    @Enumerated(EnumType.STRING)
    public LogicalDocumentType getLogicalDocumentType() {
        return logicalDocumentType;
    }

    public void setLogicalDocumentType(LogicalDocumentType logicalDocumentType) {
        this.logicalDocumentType = logicalDocumentType;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "bookingId")
    @JsonIgnore
    public Booking getBooking() {
        return booking;
    }

    public void setBooking(Booking booking) {
        this.booking = booking;
    }

    @Override
    public boolean equals(final Object o) {
        if (!(o instanceof PassengerDocument)) {
            return false;
        }
        if (this == o) {
            return true;
        }
        return id != null && id.equals(((PassengerDocument) o).getId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, getDocumentType(), getStatus(), getLogicalDocumentType(), getPassengerReference(), getPassengerTypology(), getOriginalDocumentId(), getDocumentName(), getS3DocumentName());
    }

    @Enumerated(EnumType.STRING)
    public DccValidationStatus getStatus() {
        return status;
    }

    public void setStatus(DccValidationStatus status) {
        this.status = status;
    }
}
