import { DccValidationStatus } from "../dcc/dcc-validation-status.enum";
import { LogicalDocumentType } from "./logical-document-type.enum";

export interface BookingPassengerDocumentResource {
  id: number;
  sequenceNumber: number;
  pageIndex: number;
  passengerReference: string;
  passengerTypology: string;
  documentName: string;
  documentType: string;
  original: boolean;
  convertedDocuments?: BookingPassengerDocumentResource[];
  exemptionReason?: string;
  logicalDocumentType?: LogicalDocumentType;
  status?: DccValidationStatus;
}

export interface BookingPassengerDocumentContentResource {
  id: number;
  contentFound: boolean;
  bookingId: string;
  passengerReference: string;
  passengerTypology: string;
  documentImage: string;
  firstName: string;
  lastName: string;
}

export interface PassengerDocumentValidationResultResource {  
  bookingId: string;
  accept: boolean;
  remark?: string;
}

