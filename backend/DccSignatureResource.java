public class DccSignatureResource {
        @JsonProperty(value = "isValid")
        private boolean isValid;
        private String status;

        public boolean isValid() {
                return isValid;
        }

        public void setValid(boolean valid) {
                isValid = valid;
        }

        public String getStatus() {
                return status;
        }

        public void setStatus(String status) {
                this.status = status;
        }
}
