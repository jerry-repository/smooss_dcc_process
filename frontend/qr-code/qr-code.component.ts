import { Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges } from '@angular/core';
import { AnimationUtils, ArrayUtils, BookingPassengerDocumentContentResource, BookingPassengerDocumentResource, BookingPassengerResource, BookingResource, BookingUtils, ChoiceType, ContentResource, ContentUtils, LogicalDocumentType, SmoossConstants } from '@jerry/model';
import { DccValidationStatus } from 'libs/model/src/dcc/dcc-validation-status.enum';

@Component({
  selector: 'jerry-offers-panel-qr-code',
  templateUrl: './qr-code.component.html',
  styleUrls: ['./qr-code.component.scss'],
  animations: AnimationUtils.rotationAnimations()
})
export class OffersPanelQrCodeComponent implements OnInit, OnChanges {
  
  @Input() booking: BookingResource;
  @Input() passengerDocumentContent: BookingPassengerDocumentContentResource;
  @Input() offerType: ChoiceType;

  @Output() emitPreviewPassengerDocument = new EventEmitter();

  documentsByPax: Map<BookingPassengerResource, BookingPassengerDocumentResource[]>;
  displayedImage: string;
  offerContentName: string;
  offerContentDescription: string;
  currentDocumentPageIndex: number;
  totalDocumentPages: number;
  displayDocumentsPagingBlock: boolean; 
  pagingPreviousPossible: boolean; 
  pagingNextPossible: boolean;
  currentOriginalDocumentSelected: BookingPassengerDocumentResource;
  currentConvertedDocumentPreviewed: BookingPassengerDocumentResource;
  isImageLoading: boolean;
  isOfferDescriptionDisplayed: boolean;

  // Structure defined in AnimationUtils
  currentAnimationState: {
    value: string,
    params?: {
      coefficient: number,
      width: number,
      height: number
    }
  };

  constructor() {
    this.documentsByPax = new Map<BookingPassengerResource, BookingPassengerDocumentResource[]>();
    this.isImageLoading = false;
    this.isOfferDescriptionDisplayed = false;
  }
  
  // --------------------------------------------------
  // Lifecycle Hooks
  // --------------------------------------------------

  ngOnChanges(changes: SimpleChanges) {
    if (changes) {
      if (changes.booking && changes.booking.currentValue) {
        this.booking = changes.booking.currentValue;

        this._initOfferContent();
        this._initPassengerDocuments();
      }

      if (changes.passengerDocumentContent && changes.passengerDocumentContent.currentValue) {
        this._previewPassengerDocument(changes.passengerDocumentContent.currentValue);
      }
    }
  }

  ngOnInit(): void {
    this.displayDocumentsPagingBlock = false;
    this._checkPagingArrows();
  }

  // --------------------------------------------------
  // Publics Methods
  // --------------------------------------------------

  onSelectDocument(originalDocument: BookingPassengerDocumentResource): void {
    this.currentOriginalDocumentSelected = originalDocument;
    
    if (LogicalDocumentType.DCC_CONTENT !== originalDocument.logicalDocumentType) {
      const convertedDocumentsExist = ArrayUtils.isNotEmpty(originalDocument.convertedDocuments);
      this.displayDocumentsPagingBlock = convertedDocumentsExist;
  
      if (convertedDocumentsExist) {
        // Init paging informations
        this.currentDocumentPageIndex = 1;
        this.totalDocumentPages = originalDocument.convertedDocuments.length;
        if (this.totalDocumentPages === 1) {
          this.displayDocumentsPagingBlock = false;
        }
        this._onSelectConvertedDocument();
      } else {
        this.emitPreviewPassengerDocument.emit(originalDocument);
      }
    } else {
      this.displayedImage = null;
    }
  }

  onPreviewPreviousDocument(): void {
    if (this.currentDocumentPageIndex > 1) {
      this.currentDocumentPageIndex -= 1;
    }

    this._onSelectConvertedDocument();
  }

  onPreviewNextDocument(): void {
    if (this.currentDocumentPageIndex < this.totalDocumentPages) {
      this.currentDocumentPageIndex += 1;
    }

    this._onSelectConvertedDocument();
  }

  onToggleOfferDescription(isOpened: boolean): void {
    this.isOfferDescriptionDisplayed = isOpened;
  }

  onRotateImage(parentWidth: number): void {
    let currentCoefficient = 0;
    let isAxesReversed = false;

    if (this.currentAnimationState) {
      // 1/4 turn
      currentCoefficient = this.currentAnimationState.value !== AnimationUtils.INIT ? this.currentAnimationState.params.coefficient + 0.25 : 0.25;
      // Left or right rotation => widths and heights are reversed
      isAxesReversed = [0.25, 0.75].includes(Math.abs(currentCoefficient) - Math.floor(currentCoefficient));
    }

    this.currentAnimationState = { 
      value: isAxesReversed ? AnimationUtils.REVERSED_AXES : AnimationUtils.DEFAULT_AXES,
      params: {
        coefficient: currentCoefficient,
        width: isAxesReversed ? 0 : parentWidth,
        height: isAxesReversed ? parentWidth : 0
      }
    };
  }

  getLogicalDocumentTypeDesc(logicalDocumentType: LogicalDocumentType): string {
    switch (logicalDocumentType) {
      case LogicalDocumentType.ADDITIONAL_DOCUMENT:
        return 'Documents further to DCC';
      case LogicalDocumentType.EU_HEALTH_PASS:
        return 'DCC uploaded by passenger';
      case LogicalDocumentType.FOREIGN_HEALTH_PASS:
        return 'Certificate substituting to DCC';
      case LogicalDocumentType.PROOF:
        return 'Proof for passenger\'s name mismatch';
      default:
        return 'N/A';
    }
  }

  // --------------------------------------------------
  // Private Methods
  // --------------------------------------------------
  
  private _initOfferContent() {
    this.offerContentName = ContentUtils.getContentForSelectedOfferInBooking(this.booking, SmoossConstants.CONTENT_NAME);
    this.offerContentDescription = ContentUtils.getContentForSelectedOfferInBooking(this.booking, SmoossConstants.CONTENT_TEXT_DESCRIPTION);
  }

  private _initPassengerDocuments() {
    this.documentsByPax = new Map<BookingPassengerResource, BookingPassengerDocumentResource[]>();
    // Init Document map
    this.booking.passengers.forEach(passenger => {
      const key = BookingUtils.buildBookingPassengerKey(passenger);

      if (this.booking.originalPassengerDocuments) {
        const passengerDocuments: BookingPassengerDocumentResource[] = this.booking.originalPassengerDocuments
          .filter(passengerDocument => {
            const docPaxKey = BookingUtils.buildBookingPassengerKeyByRefAndType(passengerDocument.passengerReference, passengerDocument.passengerTypology);
            return docPaxKey === key;
          });

        this.documentsByPax.set(passenger, passengerDocuments);
      }
    })
  }
  
  private _previewPassengerDocument(passengerDocumentContent: BookingPassengerDocumentContentResource) {
    // Init image rotation
    this.currentAnimationState = { value: AnimationUtils.INIT };

    this.displayedImage = passengerDocumentContent.documentImage;  
    this.isImageLoading = false;
  }

  private _checkPagingArrows(): void {
    if (!this.displayDocumentsPagingBlock) {
      this.pagingPreviousPossible = false;
      this.pagingNextPossible = false;
    } else {
      this.pagingPreviousPossible = true;
      this.pagingNextPossible = true;
      if (this.currentDocumentPageIndex === 1) {
        this.pagingPreviousPossible = false;      
      }
      if (this.currentDocumentPageIndex === this.totalDocumentPages) {
        this.pagingNextPossible = false;      
      }
    }
  }

  private _onSelectConvertedDocument() {  
    this.isImageLoading = true;
    this._checkPagingArrows();

    this.currentConvertedDocumentPreviewed = this.currentOriginalDocumentSelected.convertedDocuments[this.currentDocumentPageIndex-1];
    this.emitPreviewPassengerDocument.emit(this.currentConvertedDocumentPreviewed);
  }
}