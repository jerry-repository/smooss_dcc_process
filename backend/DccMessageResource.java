public class DccMessageResource {
    private String type;
    private String label;
    private List<DccFieldResource> fields;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public List<DccFieldResource> getFields() {
        return fields;
    }

    public void setFields(List<DccFieldResource> fields) {
        this.fields = fields;
    }
}
