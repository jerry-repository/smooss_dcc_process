@Service
public class BookingServiceImpl implements BookingService {
    @Override
    @Transactional
    public Booking saveBooking(Booking booking) {
        booking.setLastUpdate(LocalDateTime.now());
        if (!CollectionUtils.isEmpty(booking.getTransactions())) {
            booking.getTransactions().forEach(bookingTransaction -> {
                if (bookingTransaction.getId() == null) {
                    bookingTransaction.setLastUpdate(LocalDateTime.now());
                }
            });
        }
        return bookingRepository.save(booking);
    }

    @Override
    public void createPassengerDocument(Booking booking, PassengerDocument passengerDocument) {
        // find if a DCC already exists for the current pax reference
        PassengerDocument docToReplace = booking.getPassengerDocumentsList().stream()
                .filter(currentPassengerDocument -> currentPassengerDocument.getPassengerReference().equals(passengerDocument.getPassengerReference())
                        && LogicalDocumentType.DCC_CONTENT.equals(currentPassengerDocument.getLogicalDocumentType())
                        && !DccValidationStatus.TRIP_VALID.equals(currentPassengerDocument.getStatus()))
                .findFirst().orElse(null);

        // if a DCC exist, replace with the current document
        if (docToReplace != null) {
            passengerDocument.setId(docToReplace.getId());
            booking.removePassengerDocument(docToReplace);
        }

        passengerDocument.setLastUpdate(LocalDateTime.now());
        booking.addPassengerDocuments(passengerDocument);
    }

    @Override
    public boolean alreadyContainsDccValidation(Passenger currentPax, String bookingId) {
        return !CollectionUtils.isEmpty(passengerDocumentRepository.findByBookingIdAndPassengerReferenceAndLogicalDocumentTypeAndStatus(bookingId, currentPax.getReference(), LogicalDocumentType.DCC_CONTENT, DccValidationStatus.TRIP_VALID));
    }

    @Override
    public void changeBookingStatusToProcessed(Booking booking) {
        if (ChoiceType.QR_CODE.equals(booking.getSelectedType())) {
            // If all pax have valid DCC => PROCESSED
            List<String> passengerDocumentsDcc = booking.getPassengerDocuments().stream().filter(passengerDocument -> LogicalDocumentType.DCC_CONTENT.equals(passengerDocument.getLogicalDocumentType())
                    && passengerDocument.getStatus() != null
                    && DccValidationStatus.TRIP_VALID.equals(passengerDocument.getStatus()))
                    .map(PassengerDocument::getPassengerReference)
                    .collect(Collectors.toList());

            boolean toProcessed = booking.getBookingSnapshotsList().get(0).getPassengersList().stream()
                    .filter(passenger -> !PassengerType.INF.equals(passenger.getTypology()) && !PassengerType.CHD.equals(passenger.getTypology()))
                    .allMatch(passenger -> passengerDocumentsDcc.contains(passenger.getReference()));

            if (toProcessed) {
                BookingUtils.changeBookingStatus(booking, BookingStatus.PROCESSED);
            }
        }

    }

    }

    @Override
    public boolean acceptPassengerDocumentsInBooking(Campaign campaign, Target target, Booking booking, String userId) {
        try (JsonThreadNaming targetThread = new JsonThreadNaming(campaign, target, null, Thread.currentThread().getName())) {
            try (JsonThreadNaming oThread = new JsonThreadNaming(campaign, target, booking, Thread.currentThread().getName())) {
                try {
                    LOG.info("Accept passengers documents");
                    // Add SK remarks in the PNR
                    String pnrNumber = booking.getPnr(); // CollectionUtils.firstElement(booking.getBookingSnapshotsList()).getPnr() ?
                    if (pssService.setRemarksOnTargetSlice(pnrNumber, target, booking, RemarkContext.DOCUMENTS_ACCEPTED)) {
                        Integer nbDocumentsDeleted = deletePassengerDocumentsByBooking(booking);
                        if (nbDocumentsDeleted == null) {
                            LOG.warn("No documents have been deleted");
                        }
                        return true;
                    }
                } catch (Exception e) {
                    LOG.error("Error occurred while accepting passenger documents : " + e.getMessage(), e);
                    BookingUtils.changeBookingStatus(booking, BookingStatus.ERROR, userId);

                    // Send Error Email
                    this.handleBookingEvent(campaign, target, null, booking, null, null);
                }
            }
        }
        return false;
    }
}


@Override
public void createPassengerDocument(Booking booking, PassengerDocument passengerDocument) {
    // find if a DCC already exists for the current pax reference
    PassengerDocument docToReplace = booking.getPassengerDocumentsList().stream()
            .filter(currentPassengerDocument -> currentPassengerDocument.getPassengerReference().equals(passengerDocument.getPassengerReference())
                    && LogicalDocumentType.DCC_CONTENT.equals(currentPassengerDocument.getLogicalDocumentType())
                    && !DccValidationStatus.TRIP_VALID.equals(currentPassengerDocument.getStatus()))
            .findFirst().orElse(null);

    // if a DCC exist, replace with the current document
    if (docToReplace != null) {
        passengerDocument.setId(docToReplace.getId());
        booking.removePassengerDocument(docToReplace);
    }

    passengerDocument.setLastUpdate(LocalDateTime.now());
    booking.addPassengerDocuments(passengerDocument);
}