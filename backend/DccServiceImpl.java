public class DccServiceImpl implements DccService {

    private static final Log LOG = LogFactory.getLog(DccServiceImpl.class);
    private static final String MATCH_ANY_ASCII = "\\p{ASCII}";
    private static final String MATCH_ANY_LETTERS = "a-zA-Z";

    @Value("${dcc.token}")
    private String token;

    @Value("${dcc.host}")
    private String host;

    @Value("${dcc.publicKey}")
    private String serverPublicKey;

    @Value("${dcc.alias}")
    private String alias;

    @Autowired
    private GeographicalDataService geographicalDataService;

    private static final String VALID_DCC_STATUS = "Valide";
    private static final String BLACKLISTED_DCC_STATUS = "Certificat Frauduleux";

    @Override
    public DccValidationStatus isValidDcc(String dccValue, Passenger passenger, Segment targetedSegment) {
        if (StringUtils.isEmpty(passenger.getLastName())) {
            return DccValidationStatus.UNEXPECTED_ERROR;
        }
        String originCountry = geographicalDataService.getCountryCodeByStation(targetedSegment.getOriginCode());
        String destinationCountry = geographicalDataService.getCountryCodeByStation(targetedSegment.getDestinationCode());
        DccRootResource dccInformation = retrieveDccInformation(dccValue, targetedSegment.getDepartureDateTimeLt(), originCountry, destinationCountry);
        if (dccInformation == null) {
            return DccValidationStatus.UNRECOGNIZED_QR;
        }

        boolean blacklisted = dccInformation.getData().getDynamic().stream().anyMatch(dynamic -> BLACKLISTED_DCC_STATUS.equals(dynamic.getLiteHealthPassValidityStatus()));
        if (blacklisted) {
            return DccValidationStatus.BLACKLISTED;
        }

        boolean dccValidity = dccInformation.getData().getDynamic().stream().anyMatch(dynamic -> VALID_DCC_STATUS.equals(dynamic.getLiteHealthPassValidityStatus()));
        if (!dccValidity) {
            return DccValidationStatus.TRIP_INVALID;
        }

        boolean nameCompatibility = checkNameCompatibility(dccInformation, passenger);
        if (!nameCompatibility) {
            return DccValidationStatus.INVALID_NAME;
        }

        return DccValidationStatus.TRIP_VALID;
    }

    @Override
    public DccRootResource retrieveDccInformation(String dccValue, LocalDateTime departureDate, String originCountry, String destinationCountry) {
        // Encode dccValue, generate RSA key
        DccOverEncryptionDetails dccOverEncryptionDetails = getOverEncryptionDetails(dccValue);
        if (dccOverEncryptionDetails == null) {
            return null;
        }
        HttpResponse<String> response;
        try {
            response = Unirest.post(host + "api/document/v2/DCC?controlDate=" + departureDate.toString() +
                            "&publicKey=" + dccOverEncryptionDetails.getUrlEncodedPublicKey() +
                            "&keyAlias=" + alias + "&fromCountry=" + originCountry +
                            "&toCountry=" + destinationCountry)
                    .header("Content-Type", "text/plain")
                    .header("accept", "*/*")
                    .header("Authorization", "Bearer " + token)
                    .body(dccOverEncryptionDetails.getEncryptDccValue())
                    .asString();
        } catch (UnirestException e) {
            LOG.error("Error occurred during retrieve Dcc information", e);
            return null;
        } catch (UnsupportedEncodingException e) {
            LOG.error("Error occurred during retrieve client public key information", e);
            return null;
        }
        if (response.getStatus() != 200) {
            LOG.error("Error occurred during retrieve Dcc information, status Code : " + response.getStatus());
            return null;
        }
        try {
            ObjectMapper om = new ObjectMapper();
            return om.readValue(EccUtils.decryptMessage(response.getBody(), dccOverEncryptionDetails.getIv(), serverPublicKey, dccOverEncryptionDetails.getEphemeralKey().getPrivate()), DccRootResource.class);
        } catch (JsonProcessingException e) {
            LOG.error("Error occurred during mapping of Dcc information", e);
            return null;
        } catch (UnsupportedEncodingException | NoSuchAlgorithmException | InvalidKeySpecException | NoSuchProviderException | InvalidKeyException | InvalidCipherTextException e) {
            LOG.error("Error occurred during decrypt  Dcc information", e);
            return null;
        }
    }

    private DccOverEncryptionDetails getOverEncryptionDetails(String dccValue) {
        try {
            DccOverEncryptionDetails dccOverEncryptionDetails = new DccOverEncryptionDetails();
            dccOverEncryptionDetails.setEphemeralKey(EccUtils.generateEphemeralKeyPair());
            dccOverEncryptionDetails.setIv(RandomStringUtils.randomAlphanumeric(EccUtils.IV_LENGTH));
            dccOverEncryptionDetails.setEncryptDccValue(EccUtils.encryptMessage(dccValue, dccOverEncryptionDetails.getIv(), serverPublicKey, dccOverEncryptionDetails.getEphemeralKey().getPrivate()));
            return dccOverEncryptionDetails;
        } catch (UnsupportedEncodingException | NoSuchAlgorithmException | InvalidKeySpecException | NoSuchProviderException | InvalidKeyException | InvalidCipherTextException e) {
            LOG.error("Error occurred during retrieve over encryption information", e);
            return null;
        }
    }

    private boolean checkNameCompatibility(DccRootResource dccRootResource, Passenger passenger) {

        String dccLastName = dccRootResource.getData().getDynamic().get(0).getLiteLastName();
        String dccFirstName = dccRootResource.getData().getDynamic().get(0).getLiteFirstName();
        String pnrLastName = passenger.getLastName();
        String pnrFirstName = passenger.getFirstName();

        String dccLastNameWithOnlyAlphabet = Normalizer.normalize(dccLastName, Normalizer.Form.NFD)
                .replaceAll("[^" + MATCH_ANY_ASCII + "]", "");


        String dccFirstNameWithOnlyAlphabet = Normalizer.normalize(dccFirstName, Normalizer.Form.NFD)
                .replaceAll("[^" + MATCH_ANY_ASCII + "]", "");


        String pnrLastNameWithOnlyAlphabet = Normalizer.normalize(pnrLastName, Normalizer.Form.NFD)
                .replaceAll("[^" + MATCH_ANY_ASCII + "]", "");


        String pnrFirstNameWithOnlyAlphabet = Normalizer.normalize(pnrFirstName, Normalizer.Form.NFD)
                .replaceAll("[^" + MATCH_ANY_ASCII + "]", "");


        dccLastNameWithOnlyAlphabet = dccLastNameWithOnlyAlphabet.replaceAll("[^" + MATCH_ANY_LETTERS + "]+", "");
        dccFirstNameWithOnlyAlphabet = dccFirstNameWithOnlyAlphabet.replaceAll("[^" + MATCH_ANY_LETTERS + "]+", "");
        pnrLastNameWithOnlyAlphabet = pnrLastNameWithOnlyAlphabet.replaceAll("[^" + MATCH_ANY_LETTERS + "]+", "");
        pnrFirstNameWithOnlyAlphabet = pnrFirstNameWithOnlyAlphabet.replaceAll("[^" + MATCH_ANY_LETTERS + "]+", "");


        return (StringUtils.containsIgnoreCase(pnrLastNameWithOnlyAlphabet, dccLastNameWithOnlyAlphabet) || StringUtils.containsIgnoreCase(dccLastNameWithOnlyAlphabet, pnrLastNameWithOnlyAlphabet))
                && (StringUtils.containsIgnoreCase(pnrFirstNameWithOnlyAlphabet, dccFirstNameWithOnlyAlphabet) || StringUtils.containsIgnoreCase(dccFirstNameWithOnlyAlphabet, pnrFirstNameWithOnlyAlphabet));
    }


}
