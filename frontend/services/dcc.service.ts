import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { ConfigurationService, CustomerConfiguration } from '../configuration/configuration.service';
import { switchMap } from 'rxjs/operators';
import { DccAnswerResource, DccResponse } from "@jerry/model";

@Injectable({ providedIn: 'root' })
export class DccService {

  constructor(
    private http: HttpClient,
    private configurationService: ConfigurationService
  ) { }

  // --------------------------------------------------
  // Methods
  // --------------------------------------------------

  checkDcc(dccRequest: DccAnswerResource): Observable<DccResponse> {
    return this.configurationService.configuration$.pipe(
      switchMap((config: CustomerConfiguration) =>
        this.http.put<DccResponse>(`${config.apiUrl}customer/dcc`, dccRequest)
      )
    );
  }
}