public final class EccUtils {

    public static final int IV_LENGTH = 16;
    private static final byte[] CONVERSION = "conversion".getBytes(StandardCharsets.UTF_8);

    static {
        Security.addProvider(new org.bouncycastle.jce.provider.BouncyCastleProvider());
    }

    public static String decrypt(byte[] sharedKey, String payload, String iv) throws IllegalStateException, InvalidCipherTextException {
        byte[] clearPayload = cipherBuffer(false,
                sharedKey,
                Base64.getDecoder().decode(iv),
                Base64.getDecoder().decode(payload.substring(IV_LENGTH)));
        return new String(clearPayload);
    }

    public static String encrypt(byte[] sharedKey, String payload, String iv) throws IllegalStateException, InvalidCipherTextException {
        byte[] cipherPayload = cipherBuffer(true,
                sharedKey,
                Base64.getDecoder().decode(iv),
                payload.getBytes());
        return iv + Base64.getEncoder().encodeToString(cipherPayload);
    }

    public static byte[] calculateSharedKey(PrivateKey privateKey, PublicKey pubKey) throws NoSuchAlgorithmException, NoSuchProviderException, InvalidKeyException, InvalidKeySpecException, UnsupportedEncodingException {

        // Do the initial key agreement
        KeyAgreement ka = KeyAgreement.getInstance("ECDH", "BC");
        ka.init(privateKey);
        ka.doPhase(pubKey, true);
        byte[] sharedSecret = ka.generateSecret();

        // Apply the key derivation function
        var hmac = new HMac(new SHA256Digest());
        hmac.init(new KeyParameter(sharedSecret));

        byte[] conversionBuffer = CONVERSION;
        hmac.update(conversionBuffer, 0, conversionBuffer.length);
        byte[] result = new byte[32];
        hmac.doFinal(result, 0);

        return result;
    }

    public static byte[] calculateSharedKey(PrivateKey privateKey, String pubKey) throws NoSuchAlgorithmException, NoSuchProviderException, InvalidKeyException, InvalidKeySpecException, UnsupportedEncodingException {
        return calculateSharedKey(privateKey, string2PublicKey(pubKey));
    }

    private static byte[] cipherBuffer(boolean forEncryption, byte[] key, byte[] iv, byte[] payload) throws IllegalStateException, InvalidCipherTextException {
        // Use the shared key as the key for the decryption and the iv, ... as the iv
        GCMBlockCipher cipher = new GCMBlockCipher(new AESEngine());
        AEADParameters parameters = new AEADParameters(new KeyParameter(key), 128, iv);

        cipher.init(forEncryption, parameters);

        byte[] out = new byte[cipher.getOutputSize(payload.length)];
        int bytesWritten = cipher.processBytes(payload, 0, payload.length, out, 0);
        cipher.doFinal(out, bytesWritten);

        return out;
    }

    private static ECPublicKey string2PublicKey(String pubStr) throws NoSuchAlgorithmException, NoSuchProviderException, InvalidKeySpecException {
        byte[] keyBytes = Base64.getDecoder().decode(pubStr.getBytes());
        X509EncodedKeySpec keySpec = new X509EncodedKeySpec(keyBytes);
        KeyFactory keyFactory = KeyFactory.getInstance("EC", "BC");

        return (ECPublicKey) keyFactory.generatePublic(keySpec);
    }

    public static String encryptMessage(String clientMessage, String iv, String serverPublicKeyPEM, PrivateKey privateKey) throws UnsupportedEncodingException, NoSuchAlgorithmException, InvalidKeySpecException, NoSuchProviderException, InvalidKeyException, InvalidCipherTextException {
        byte[] sharedKey = calculateSharedKey(privateKey, serverPublicKeyPEM);
        return encrypt(sharedKey, clientMessage, iv);
    }

    public static String decryptMessage(String serverMessageEncoded, String iv, String serverPublicKeyPEM, PrivateKey privateKey) throws UnsupportedEncodingException, NoSuchAlgorithmException, InvalidKeySpecException, NoSuchProviderException, InvalidKeyException, InvalidCipherTextException {
        byte[] sharedKey = calculateSharedKey(privateKey, serverPublicKeyPEM);
        return decrypt(sharedKey, serverMessageEncoded, iv);
    }

    public static KeyPair generateEphemeralKeyPair() throws NoSuchAlgorithmException {
        // Generate ephemeral ECDH keypair
        KeyPairGenerator kpg = KeyPairGenerator.getInstance("EC");
        kpg.initialize(256);
        return kpg.generateKeyPair();
    }
}
