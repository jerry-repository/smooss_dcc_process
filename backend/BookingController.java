/**
     * Persist the result of passenger documents validation and process corresponding operations on the booking
     *
     * @param validationResultResource validationResultResource
     * @param principal                User triggering the request
     */
    @Operation(summary = "Process validation linked with passenger documents")
    @PostMapping(
            value = "/passenger/document/validation",
            produces = {MediaType.APPLICATION_JSON_VALUE})
    @ResponseBody
    public BookingResource validatePassengerDocuments(@RequestBody PassengerDocumentValidationResultResource validationResultResource, Principal principal) {
        try {
            String userId = principal.getName();
            Booking booking = bookingService.findByIdWithChildEntities(validationResultResource.getBookingId(), false);
            if (booking != null) {
                // Load Campaign and Target
                Campaign campaign = campaignService.getCampaign(booking.getCampaignId());
                if (campaign == null) {
                    throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Campaign " + booking.getCampaignId() + " Not Found");
                }
                Target target = targetService.findById(booking.getTargetId());
                if (target == null) {
                    throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Target " + booking.getTargetId() + " Not Found");
                }

                if (validationResultResource.isAccept()) {
                    bookingService.acceptPassengerDocumentsInBooking(campaign, target, booking, principal.getName());
                    booking.setRemark(null);
                    BookingUtils.changeBookingStatus(booking, BookingStatus.PROCESSED, userId);
                } else {
                    String remarkSanitize = Jsoup.clean(validationResultResource.getRemark(), Safelist.none());
                    String remarkSanitizeDecode = Jsoup.parse(remarkSanitize).text();
                    booking.setRemark(remarkSanitizeDecode);
                    BookingUtils.changeBookingStatus(booking, BookingStatus.CANCELLED, userId);
                    try (JsonThreadNaming oThread = new JsonThreadNaming(campaign, target, booking, Thread.currentThread().getName())) {
                        pssService.setRemarksOnTargetSlice(booking.getPnr(), target, booking, RemarkContext.DOCUMENTS_CANCELLED);
                    }
                }

                return finalizeBookingUpdate(booking, false, principal);
            }
        } catch (Exception e) {
            LOG.error("Error occurred while validating passengers documents", e);
        }
        throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Booking Not Found");
    }