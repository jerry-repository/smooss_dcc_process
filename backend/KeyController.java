
    @GetMapping(value = "",
            produces = {MediaType.APPLICATION_JSON_VALUE})
    public PublicKeyResponse getPublicKey() {
        if (keyService == null) {
            return null;
        }
        PublicKeyResponse keyResource = new PublicKeyResponse();
        keyResource.setPublicKey(X509_PEM_HEADER + System.getProperty("line.separator") + Base64.getEncoder().encodeToString(keyService.getRSAPublicKey().getEncoded()) + System.getProperty("line.separator") + X509_PEM_FOOTER);
        return keyResource;
    }
